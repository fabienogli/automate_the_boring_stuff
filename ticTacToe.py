empty = ' '
def printBoard(board):
    print(board['top-L'], ' | ',board['top-M'], ' | ',board['top-R'])
    print(board['mid-L'], ' | ',board['mid-M'], ' | ',board['mid-R'])
    print(board['low-L'], ' | ',board['low-M'], ' | ',board['low-R'])

def fill(board, player):
    while True:
        print('Enter a position')
        positions = availablePositions(board)
        print('Availble positions\n', positions)
        position = input()
        if position in board.keys() and board[position] == empty :
            board[position] = player
            return board
        if position not in board.keys():
            print('Oops, you seems to have typed an unavailable position')
            continue
        print('continue works')

def availablePositions(board):
    positions = []
    for i in board.keys():
        if board[i] == empty:
            positions.append(i)
    return positions


def play():
    theBoard = {'top-L': empty, 'top-M': empty, 'top-R': empty,
            'mid-L': empty, 'mid-M': empty, 'mid-R': empty,
            'low-L': empty, 'low-M': empty, 'low-R': empty}
    print(theBoard.keys())
    print(list(theBoard.keys()))
    players = ['X', 'O']
    turn = 0
    while True:
        if empty not in theBoard.values():
            print('No winner lol')
            break
        print('Player ', str(turn + 1), ' turns')
        theBoard = fill(theBoard, players[turn])
        printBoard(theBoard)
        if hasWon(theBoard, players[turn]):
            print('Player ', str(turn + 1), 'won')
            break
        turn = (turn + 1) % 2

def hasWon(board, player):
    flatBoard = list(board.values())
    #check horizontal and vertical win
    for i in range(0,3):
        horizontalWin = True
        verticalWin = True
        for y in range(0, 3):
            horizontalWin = flatBoard[i*3 + y] == player and horizontalWin
            verticalWin = flatBoard[y*3 + i] == player and verticalWin
        if horizontalWin or verticalWin:
            return True
    if flatBoard[4] == player:
        if flatBoard[0] == player and flatBoard[8] == player:
            return True
        elif flatBoard[2] == player and flatBoard[6]:
            return True
    return False


def main():
    play()

if __name__ == '__main__':
    main()
