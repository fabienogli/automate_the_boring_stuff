#!python

PASSWORDS = {'email': '15465zad54a65d46zad56za4',
            'blog': 'VHLKSHDKJHKLJS8785az',
            'luggage': 'password'}

import sys,pyperclip

if len(sys.argv) < 2:
    print('Usage: py pw.py [account] - copy account password')
    sys.exit()

account = sys.argv[1]

if account in PASSWORDS:
    pyperclip.copy(PASSWORDS[account])
    print('Password for ', account, ' copied to clipboard.')
else:
    print('There is no account named ' + account)

