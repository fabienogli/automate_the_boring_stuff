# The Collatz Sequence

def collatz(number):
    if number % 2 == 0:
        return number // 2
    return number * 3 + 1

def main():
    print('Enter number')
    try :
        number = int(input())
        while True:
            number = collatz(number)
            print(number)
            if number == 1:
                break
    except ValueError:
        print('You must enter an integer')

if __name__ == '__main__':
    main()
