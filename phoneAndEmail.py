#! python
# Search phone number and email address in your clipboard and paste it in it
import pyperclip, re

# I follow an american book so this is for american number
uscaPhoneRegex = re.compile(r'''(
    (\+\d*)?
    (\d{3}|\(\d{3}\))?
    (\s|-|\.)?
    (\d{3})
    (\s|-|\.)
    (\d{4})
    (\s*(ext|x|ext.)\s*\d{2,5})?
    )''', re.VERBOSE)


frPhoneRegex = re.compile(r'''(
    (\+)?
    (\d{2}(\s|-|\.))
    (\d{1,3}|\(\d\)\s\d)
    (\s|-|\.)?
    (\d{2,3})
    (\s|-|\.)
    (\d{2,3})
    (\s|-|\.)
    (\d{2,3})
    (\s|-|\.)
    (\d{2})?
    (\s*(ext|x|ext.)\s*\d{2,5}$)?
    )''', re.VERBOSE)

emailRegex = re.compile(r'''(
    [a-zA-Z0-9._%+-]+
    @
    [a-zA-Z0-9.-]+
    (\.[a-zA-Z]{2,4})
    )''', re.VERBOSE)

def findEmails(toSearch):
    result = emailRegex.findall(toSearch)
    emails = [email[0] for email in result]
    return emails

def findPhoneNumber(toSearch):
    numbers=[]
    fr = frPhoneRegex.findall(toSearch)
    for item in fr:
        numbers.append(item[0])
    us =  uscaPhoneRegex.findall(toSearch)
    for item in us:
        numbers.append(item[0])
    return numbers

def testNumbers():
    frFail = '415.863.99\n800.420.7240'
    print("To test:", frFail)
    print("result")
    print("\n".join(findPhoneNumber(frFail)))

    test = '+33 (0) 5 61 93 33 33\n+33 5 81 31 75 00\n+33 567 198 854 \n04 42 85 85 85\n415-555-9999'
    print("To test\n", test)
    result = findPhoneNumber(test)
    print("Result:\n", '\n'.join(result))

def main():
    toSearch = pyperclip.paste()
    numbers = findPhoneNumber(toSearch)
    emails = findEmails(toSearch)
    result = '\n'.join(numbers) + "\n" + "\n".join(emails)
    pyperclip.copy(result)
    print("Copied to clipboard;")
    print(result)

if __name__ == '__main__':
    main()


def testEmail():
    emailsTest = 'Reach Us by Email\nGeneral inquiries: info@nostarch.com\nMedia request\nmedia@nostarch.com\nAcademic requests: academic@nostarch.com (Further information)\nHelp with your order: info@nostarch.com'
    emails = findEmails(emailsTest)
    print("Email to search\n", emailsTest)
    print("Emails found:")
    print("\n".join(emails))
